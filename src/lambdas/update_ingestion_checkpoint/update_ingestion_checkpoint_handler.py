import os
from typing import Dict

from src.common.aws_requests import update_ingestion_checkpoint
from src.common.exceptions import TableNotFoundException
from src.common.log import create_logger
from src.common.models import NoticeType
from src.common.utils import get_end_date
from src.common.utils import get_env_var_value

os.environ["LOGGER_NAME"] = "update-ingestion-checkpoint-handler"
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def handle(_event, _context) -> Dict:
    try:
        logger.info(f'Method update_ingestion_checkpoint triggered with parameters : {_event}')
        notice_type = NoticeType(_event['notice_type'])
        ingestion_checkpoint_key = notice_type.value
        end_date = get_end_date()
        update_ingestion_checkpoint(ingestion_checkpoint_key, end_date)
        return {"statusCode": 200, "body": f"Ingestion checkpoint for {notice_type.value}(s) updated"}
    except TableNotFoundException as err:
        raise err
    except Exception as err:
        logger.error(f'Error during the retrieval of notices ids, ingestion checkpoint not updated : {err}')