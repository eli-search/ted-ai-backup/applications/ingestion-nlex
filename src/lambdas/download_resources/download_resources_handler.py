import os
from typing import Dict

from src.common.aws_requests import get_ssm_parameter
from src.common.config import SSM_S3_INPUT_BUCKET_ID
from src.common.exceptions import TableNotFoundException
from src.common.log import create_logger
from src.common.models import ResourceType
from src.common.utils import get_env_var_value
from src.lambdas.download_resources.notice_service import process_notice_ids
from src.lambdas.download_resources.procurement_service import process_procurement_urls

os.environ["LOGGER_NAME"] = "download-resources-handler"
os.environ["INPUT_BUCKET"] = get_ssm_parameter(SSM_S3_INPUT_BUCKET_ID)
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def handle(_event, _context) -> Dict:
    logger.info(f'Method download_resources triggered with parameters : {_event}')
    messages = _event['Records']
    try:
        notice_ids = [message['body'] for message in messages if
                      message['messageAttributes']['type']['stringValue'] == ResourceType.NOTICE.value]
        procurement_urls = [message['body'] for message in messages if
                            message['messageAttributes']['type']['stringValue'] == ResourceType.PROCUREMENT.value]
        if notice_ids:
            process_notice_ids(notice_ids)
        if procurement_urls:
            process_procurement_urls(procurement_urls)
        return {"statusCode": 200,
                "body": f"{len(notice_ids)} notice(s) and {len(procurement_urls)} procurement URL(s) processed"}
    except TableNotFoundException as err:
        raise err
