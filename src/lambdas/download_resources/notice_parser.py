import logging
import os
from dataclasses import asdict
from pprint import pprint

from lxml import etree
from lxml.etree import _Element

from src.common import utils
from src.common.models import IngestionNotice
from src.common.utils import normalize_notice_id, is_valid_etendering_url


NOTICE_ID_XPATH = "//x:NOTICE_DATA/x:NO_DOC_OJS/text()"
PUB_DATE_XPATH = "//x:CODED_DATA_SECTION/x:REF_OJS/x:DATE_PUB/text()"
REF_NOTICE_ID_XPATH = "//x:REF_NOTICE/x:NO_DOC_OJS/text()"
NOTICE_TYPE_XPATH = "//x:TD_DOCUMENT_TYPE/text()"
ETENDERING_URL_XPATH = "//x:IA_URL_ETENDERING/text()"


class NoticeParser:
    xml: _Element
    namespaces: dict

    def __init__(self, xml: _Element):
        self.xml = xml
        self.namespaces = {'x': xml.nsmap[None]}
        self.logger = logging.getLogger(utils.get_env_var_value("LOGGER_NAME"))

    def parse(self) -> dict:
        notice_id = self.get_notice_id()
        notice_type = self.get_notice_type()
        notice_publication_date = self.get_pub_date()
        notice_reference_notice_id = self.get_ref_notice_id()
        etendering_url = self.get_etendering_url()
        notice = IngestionNotice(
                id=notice_id,
                type=notice_type,
                publication_date=notice_publication_date,
                reference_notice_id=notice_reference_notice_id,
                namespaces=self.namespaces,
                etendering_url=etendering_url,
                xml_str=""
        )
        self.logger.info(f'Finished parsing the notice.')
        return asdict(notice)

    def get_notice_id(self) -> str:
        self.logger.info(f"Retrieving notice ID with XPATH: {NOTICE_ID_XPATH}")
        notice_id = self.xml.xpath(NOTICE_ID_XPATH, namespaces=self.namespaces)
        return normalize_notice_id(notice_id[0]) if len(notice_id) else ''

    def get_pub_date(self) -> str:
        self.logger.info(f"Retrieving publication date with XPATH: {PUB_DATE_XPATH}")
        date = self.xml.xpath(PUB_DATE_XPATH, namespaces=self.namespaces)
        return date[0] if len(date) else ''

    def get_ref_notice_id(self) -> str:
        self.logger.info(f"Retrieving reference notice ID with XPATH: {REF_NOTICE_ID_XPATH}")
        ref = self.xml.xpath(REF_NOTICE_ID_XPATH, namespaces=self.namespaces)
        return normalize_notice_id(ref[0]) if len(ref) else ''

    def get_notice_type(self) -> str:
        self.logger.info(f"Retrieving notice type with XPATH: {NOTICE_TYPE_XPATH}")
        notice_type = self.xml.xpath(NOTICE_TYPE_XPATH, namespaces=self.namespaces)
        return notice_type[0] if len(notice_type) else ''

    def get_etendering_url(self) -> str:
        self.logger.info(f"Retrieving eTendering URL with XPATH: {ETENDERING_URL_XPATH}")
        url_result = self.xml.xpath(ETENDERING_URL_XPATH, namespaces=self.namespaces)
        url = url_result[0] if len(url_result) else ''
        return url if is_valid_etendering_url(url) else ''


if __name__ == '__main__':
    os.environ["LOGGER_NAME"] = "notice_parser"
    with open('../../../data/notices/296454-2023.xml') as f:
        content = f.read()
        xml = etree.fromstring(bytes(content, encoding='utf-8'))
        print(f"Extracted the following namespaces:")
        pprint(xml.nsmap)
        namespace = xml.nsmap[None]
        try:
            if namespace:
                parser = NoticeParser(xml)
                parsed_notice = parser.parse()
                if parsed_notice:
                    print(f"contract notice successfully parsed.")
                    pprint(parsed_notice)
                else:
                    print(f"Could not parse notice")
        except Exception as err:
            print(f'Error during parsing : {err}')
