import os
from typing import Dict
from typing import List

from src.common.aws_requests import get_ingestion_checkpoint
from src.common.aws_requests import insert_resources_in_ingestion_table
from src.common.config import MAX_PAGE_SIZE
from src.common.exceptions import TableNotFoundException, ScanTableException
from src.common.log import create_logger
from src.common.models import NoticeType
from src.common.models import ResourceType
from src.common.ted_client import get_notices_from_ted_api
from src.common.utils import get_api_filters
from src.common.utils import get_end_date
from src.common.utils import get_env_var_value
from src.common.utils import get_start_date

os.environ["LOGGER_NAME"] = "get-notices-ids-handler"
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def get_notice_ids(start_date: str, end_date: str, page: int, notice_type: NoticeType) -> List[str]:
    logger.info(f'Starting notice ids retrieval : \n'
                f'From: {start_date} \n'
                f'To: {end_date} \n'
                f'Page: {page}')
    logger.info(f'Retrieving page {page}...')
    response = get_notices_from_ted_api(get_api_filters('ND,PD', page, MAX_PAGE_SIZE, start_date, end_date, notice_type))
    return [result['ND'] for result in response['results']]


def handle(_event, _context) -> Dict:
    try:
        logger.info(f'Method get_notice_ids triggered with parameters : {_event}')
        notice_type = NoticeType(_event['notice_type'])
        start_date = get_start_date(get_ingestion_checkpoint(notice_type.value), logger)
        end_date = get_end_date()
        start_page = int(_event['start_page'])
        end_page = int(_event['end_page'])
        new_notices = []
        if start_date <= end_date:
            for page in range(start_page, end_page + 1):
                new_notices.extend(get_notice_ids(start_date, end_date, page, notice_type))
            insert_resources_in_ingestion_table(new_notices, ResourceType.NOTICE)
        return {"statusCode": 200, "body": f"{len(new_notices)} {notice_type.value}(s) added to ingestion tasks."}
    except TableNotFoundException as err:
        raise err
    except ScanTableException as err:
        raise err
    except Exception as err:
        logger.error(f'Error during the retrieval of notices ids, ingestion checkpoint not updated : {err}')
