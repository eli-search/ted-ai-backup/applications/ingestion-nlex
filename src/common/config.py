PRIOR_NOTICES_QUERY_LABELS = [
    "pin-cfc-standard",
    "pin-tran",
    "pin-cfc-social",
    "pin-rtl",
    "pin-only"
]
CONTRACT_NOTICE_QUERY_LABELS = [
    "cn-social",
    "cn-standard"
]
CONTRACT_AWARD_NOTICES_QUERY_LABELS = [
    "can-standard",
    "can-social",
    "can-tran",
]
CHANGE_NOTICES_QUERY_LABELS = [
    "can-modif",
    "corr"
]
DEFAULT_START_DATE = '20140101'
MAX_PAGE_SIZE = 1000  # max 1000 for classic fields, 100 for xml content
MAX_NB_PAGES_TO_PROCESS = 100  # Max number of notices pages to process for a single Lambda
SCOPE = 3  # all notices
SSM_INGESTION_QUEUE_URL = '/tedai/sqs/ingestion_tasks_queue/url'
SSM_DYNAMODB_INGESTION_CHECKPOINT_TABLE_ID = '/tedai/dynamodb/ingestion_checkpoint/id'
SSM_DYNAMODB_INGESTION_TASKS_TABLE_ID = '/tedai/dynamodb/ingestion_tasks/id'
SSM_DYNAMODB_INGESTION_REFERENCES_TABLE_ID = '/tedai/dynamodb/ingestion_references/id'
SSM_S3_INPUT_BUCKET_ID = '/tedai/s3/input_bucket/id'
SQS_MAX_BATCH_SIZE = 10  # max currently allowed by AWS
VALID_EXTENSIONS = ['pdf', 'docx', 'doc', 'odf']
PRIOR_INFORMATION_NOTICE_LABELS = [
    "Call for expressions of interest",
    "Prior Information Notice",
    "Prior information notice without call for competition",
    "Prior information notice with call for competition",
    "Periodic indicative notice with call for competition",
    "Periodic indicative notice without call for competition",
    "pin-cfc-standard",
    "pin-tran",
    "pin-cfc-social",
    "pin-rtl",
    "pin-only"
]
CONTRACT_NOTICE_LABELS = [
    "Contract notice",
    "Prequalification notices",
    "General information",
    "Public works concession",
    "Works concession",
    "Services concession",
    "Request for proposals",
    "Not applicable",
    "Dynamic purchasing system",
    "cn-social",
    "cn-standard"
]
CONTRACT_AWARD_NOTICE_LABELS = [
    "Contract award",
    "Contract award notice",
    "Concession award notice",
    "can-standard",
    "can-social",
    "can-tran",
]
CHANGE_NOTICE_LABELS = [
    "Additional information",
    "Corrigenda",
    "Corrigendum",
    "Modification of a contract/concession during its term",
    "can-modif",
    "corr"
]
