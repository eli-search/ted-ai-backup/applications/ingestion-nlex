import boto3


def dynamodb_client():
    return boto3.client("dynamodb")


def dynamodb_resource():
    return boto3.resource('dynamodb')


def ssm_client():
    return boto3.client('ssm')


def sqs_client():
    return boto3.client('sqs')


def s3_client():
    return boto3.client('s3')
