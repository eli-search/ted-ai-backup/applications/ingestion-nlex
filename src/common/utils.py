import base64
import os
from datetime import datetime
from datetime import timedelta
from logging import Logger

from src.common.config import CHANGE_NOTICES_QUERY_LABELS
from src.common.config import CHANGE_NOTICE_LABELS
from src.common.config import CONTRACT_AWARD_NOTICES_QUERY_LABELS
from src.common.config import CONTRACT_AWARD_NOTICE_LABELS
from src.common.config import CONTRACT_NOTICE_LABELS
from src.common.config import CONTRACT_NOTICE_QUERY_LABELS
from src.common.config import DEFAULT_START_DATE
from src.common.config import PRIOR_INFORMATION_NOTICE_LABELS
from src.common.config import PRIOR_NOTICES_QUERY_LABELS
from src.common.config import SCOPE
from src.common.exceptions import MissingEnvironmentVariableException
from src.common.models import NoticeType

INVALID_KEY_CHAR = ['@', '#']


def get_env_var_value(env_variable_name: str) -> str:
    try:
        return os.environ[env_variable_name]
    except KeyError as err:
        raise MissingEnvironmentVariableException(
                f"Missing environment variable '{env_variable_name}', process stopped.") from err


def convert_base64_to_str(base64_notice: str) -> str:
    base64_bytes = base64_notice.encode('utf-8')
    notice_bytes = base64.b64decode(base64_bytes)
    return notice_bytes.decode('utf-8')


def normalize_notice_type(raw_type: str) -> NoticeType:
    if any(label == raw_type for label in PRIOR_INFORMATION_NOTICE_LABELS):
        return NoticeType.PRIOR_INFORMATION_NOTICE
    if any(label == raw_type for label in CONTRACT_NOTICE_LABELS):
        return NoticeType.CONTRACT_NOTICE
    if any(label == raw_type for label in CONTRACT_AWARD_NOTICE_LABELS):
        return NoticeType.CONTRACT_AWARD_NOTICE
    if any(label == raw_type for label in CHANGE_NOTICE_LABELS):
        return NoticeType.CHANGE_NOTICE


def normalize_notice_id(notice_id: str) -> str:
    notice_id = notice_id.strip()
    if '/' in notice_id:
        year = notice_id.split('/')[0]
        number = notice_id.split('-')[-1].lstrip('0')
    else:
        split = notice_id.split('-')
        number = split[0].lstrip('0')
        year = split[1]
    return f'{number}-{year}'


def join_list_elements_to_string(list_of_strings, separator) -> str:
    filtered_list = [element.strip() for element in list_of_strings if element]
    if filtered_list:
        return separator.join(filtered_list)
    return ''


def clean_key(key: str) -> str:
    for invalid_char in INVALID_KEY_CHAR:
        key = key.replace(invalid_char, '')
    key = key.replace(':', '_')
    return key


def clean_str(input_string: str) -> str:
    str_en = input_string.encode("ascii", "ignore")
    str_de = str_en.decode()
    invalid_chars = ['—', '-', '\'', ',', '’', '–', '"', '|', '_', '`', '[', ']', '\n']
    for char in invalid_chars:
        str_de = str_de.replace(char, '')
    return ' '.join(str_de.split())


def is_valid_etendering_url(url):
    return url and all(elt in url for elt in ['etendering', 'https'])


def get_notice_type_filter(notice_type: NoticeType) -> str:
    if notice_type == NoticeType.PRIOR_INFORMATION_NOTICE:
        return join_list_elements_to_string(PRIOR_NOTICES_QUERY_LABELS, ' or ')
    elif notice_type == NoticeType.CONTRACT_NOTICE:
        return join_list_elements_to_string(CONTRACT_NOTICE_QUERY_LABELS, ' or ')
    elif notice_type == NoticeType.CONTRACT_AWARD_NOTICE:
        return join_list_elements_to_string(CONTRACT_AWARD_NOTICES_QUERY_LABELS, ' or ')
    elif notice_type == NoticeType.CHANGE_NOTICE:
        return join_list_elements_to_string(CHANGE_NOTICES_QUERY_LABELS, ' or ')
    else:
        return ''


def get_api_filters(fields: str, page: int, size: int, start_date: str, end_date: str, notice_type: NoticeType) -> str:
    notice_type_filter = get_notice_type_filter(notice_type)
    return f'{fields}&pageNum={page}&pageSize={size}&scope={SCOPE}&q=PD=[{start_date} <> {end_date}] AND ' \
           f'notice-type=[{notice_type_filter}]'


def get_start_date(checkpoint: str, logger: Logger) -> str:
    if checkpoint:
        start_date = datetime.strptime(checkpoint, '%Y%m%d') + timedelta(days=1)
        return start_date.strftime('%Y%m%d')
    else:
        logger.warning(f'Using default start date : {DEFAULT_START_DATE}')
        return DEFAULT_START_DATE


def get_end_date() -> str:
    date = datetime.now()
    date -= timedelta(days=1)
    return date.strftime('%Y%m%d')
