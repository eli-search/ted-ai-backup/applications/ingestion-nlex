from dataclasses import dataclass
from enum import Enum
from typing import Dict


class ResourceType(Enum):
    NOTICE = "notice"
    PROCUREMENT = "procurement"


class NoticeType(Enum):
    PRIOR_INFORMATION_NOTICE = "prior_information_notice"
    CONTRACT_NOTICE = "contract_notice"
    CONTRACT_AWARD_NOTICE = "contract_award_notice"
    CHANGE_NOTICE = "change_notice"


@dataclass
class IngestionNotice:
    id: str
    type: str
    publication_date: str
    reference_notice_id: str
    namespaces: Dict[str, str]
    etendering_url: str
    xml_str: str
