from aws_lambda_powertools import Logger


def create_logger(module_name: str) -> Logger:
    new_logger = Logger(service=module_name)
    return new_logger
